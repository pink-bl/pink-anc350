#!../../bin/linux-x86_64/anc

## You may have to change anc to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/anc.dbd"
anc_registerRecordDeviceDriver pdbbase

## test env
#epicsEnvSet ("DEVIP", "127.0.0.1")
#epicsEnvSet ("DEVPORT", "2101")
#epicsEnvSet ("IOCBL", "PINK")
#epicsEnvSet ("IOCDEV", "ANC01")

drvAsynIPPortConfigure("ANCIP1","$(DEVIP):$(DEVPORT)",0,0,0)

## anc350AsynMotorCreate(<asyn port name>, <address>, <card number>, <number of axis>)
#anc350AsynMotorCreate("ANCIP1","0","0","1")

## drvAsynMotorConfigure(<assign name>, <Motor type>, <card number>, <number of axis>) 
#drvAsynMotorConfigure("ANC01", "anc350AsynMotor", "0", "1")

## Load record instances
#dbLoadTemplate("${TOP}/iocBoot/${IOC}/anc350AsynMotor.substitution")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(IOCBL):$(IOCDEV):,R=asyn,PORT=ANCIP1,ADDR=0,OMAX=256,IMAX=256")

dbLoadRecords("db/ancController.template","P=$(IOCBL):$(IOCDEV),PORT=ANCIP1")
dbLoadRecords("db/ancStepModule.template","P=$(IOCBL):$(IOCDEV),PORT=ANCIP1,ADDR=0")
dbLoadRecords("db/anc350.db","BL=$(IOCBL),DEV=$(IOCDEV),CH=ACT0")
#dbLoadRecords("db/ancStepModule.template","P=$(IOCBL):$(IOCDEV),PORT=ANCIP1,ADDR=1")
#dbLoadRecords("db/anc350.db","BL=$(IOCBL),DEV=$(IOCDEV),CH=ACT1")
#dbLoadRecords("db/ancStepModule.template","P=$(IOCBL):$(IOCDEV),PORT=ANCIP1,ADDR=2")
#dbLoadRecords("db/anc350.db","BL=$(IOCBL),DEV=$(IOCDEV),CH=ACT2")

cd "${TOP}/iocBoot/${IOC}"

## AUTOSAVE
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## AUTOSAVE
create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
